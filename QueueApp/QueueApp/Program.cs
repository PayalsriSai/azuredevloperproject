﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading.Tasks;

namespace QueueApp
{
    class Program
    {

        private const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName=articlestr;AccountKey=u5dwvvyaJOqoIeaEuVARFOGk7Srj513FyMmMv3O4bLA+DDz9v4sIb/T+V0mSgUwONEfCctmuBzW3BpQ/ylU68A==;EndpointSuffix=core.windows.net";

        //static async Task Main(string[] args)
        //{
        //    Console.WriteLine("Hello World!");

        //    CloudStorageAccount account = CloudStorageAccount.Parse(ConnectionString);

        //    CloudQueueClient client = account.CreateCloudQueueClient();

        //    CloudQueue queue = client.GetQueueReference("myqueue");
        //    await queue.CreateIfNotExistsAsync();

        //    var message = new CloudQueueMessage("your message here");

        //    await queue.AddMessageAsync(message);
        //}
        //static void Main(string[] args)
        //{
        //    if (args.Length > 0)
        //    {
        //        string value = String.Join(" ", args);
        //        SendArticleAsync(value).Wait();
        //        Console.WriteLine($"Sent: {value}");
        //    }
        //}

        static async Task Main(string[] args)

        {



            if (args.Length > 0)
            {

                string value = String.Join(" ", args);
                SendArticleAsync(value).Wait();
                Console.WriteLine($"Sent: {value}");
            }
            else
            {
                string value = await ReceiveArticleAsync();
                Console.WriteLine($"Received {value}");
            }
        }

        static async Task SendArticleAsync(string newsMessage)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConnectionString);

            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            CloudQueue queue = queueClient.GetQueueReference("newsqueue");
            bool createdQueue = await queue.CreateIfNotExistsAsync();
            if (createdQueue)
            {
                Console.WriteLine("The queue of news articles was created.");
            }

            CloudQueueMessage articleMessage = new CloudQueueMessage(newsMessage);
            await queue.AddMessageAsync(articleMessage);
        }

        static async Task<string> ReceiveArticleAsync()
        {
            CloudQueue queue = GetQueue();
            bool exists = await queue.ExistsAsync();
            if (exists)
            {
                CloudQueueMessage retrievedArticle = await queue.GetMessageAsync();
                if (retrievedArticle != null)
                {
                    string newsMessage = retrievedArticle.AsString;
                    await queue.DeleteMessageAsync(retrievedArticle);
                    return newsMessage;
                }
            }

            return "<queue empty or not created>";
        }

        static CloudQueue GetQueue()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConnectionString);

            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            return queueClient.GetQueueReference("newsqueue");
        }
        
    }
}
