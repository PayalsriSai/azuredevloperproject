﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalRecordStorageApp
{
    public class PatientRecord
    {
        public string name { get; set; }

        public string imageURI { get; set; }

        public string sasToken { get; set; }
    }
}
