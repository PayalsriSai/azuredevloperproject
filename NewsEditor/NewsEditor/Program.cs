﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Threading.Tasks;

namespace NewsEditor
{
    public class Program
    {
        private const string envVarName = "DefaultEndpointsProtocol=https;AccountName=newsstory;AccountKey=fFw11ag6oOqXOfpirYF1xfExlmKQOGQ/1cqN0Xr6Ywy0FgCODkzI+hhZtnyGWXupxOAA3kNHyPam/Ou1L0IgAQ==;EndpointSuffix=core.windows.net";

        private const string containerName = "mslearn-blob-concurrency-demo";
        private const string blobName = "newsStory.txt";

        private static string connectionString;

        public static async Task Main()
        {
            // Ensure that the storage account is ready
            CloudBlobContainer container;
            try
            {
                //connectionString = Environment.GetEnvironmentVariable(envVarName);
                connectionString = envVarName;
                container = CloudStorageAccount.Parse(connectionString)
                    .CreateCloudBlobClient()
                    .GetContainerReference(containerName);

                await container.CreateIfNotExistsAsync();
            }
            catch (Exception)
            {
                var msg = $"Storage account not found. Ensure that the environment variable {envVarName}" +
                    " is set to a valid Azure Storage connection string and that the storage account exists.";
                Console.WriteLine(msg);
                return;
            }

            // First, the newsroom chief writes the story notes to the blob
            await SimulateChief();
            Console.WriteLine();

            await Task.Delay(TimeSpan.FromSeconds(2));

            // Next, two reporters begin work on the story at the same time, one starting soon after the other
            var reporterA = SimulateReporter("Reporter A", writingTime: TimeSpan.FromSeconds(12));
            await Task.Delay(TimeSpan.FromSeconds(4));
            var reporterB = SimulateReporter("Reporter B", writingTime: TimeSpan.FromSeconds(4));

            await Task.WhenAll(reporterA, reporterB);
            await Task.Delay(TimeSpan.FromSeconds(2));

            Console.WriteLine();
            Console.WriteLine("=============================================");
            Console.WriteLine();
            Console.WriteLine("Reporters have finished, here's the story saved to the blob:");

            var story = await container.GetBlockBlobReference(blobName).DownloadTextAsync();

            Console.WriteLine(story);
        }

        // This method simulates the newsroom chief writing story notes to a new blob
        // prior to authors being assigned to the story
        private static async Task SimulateChief()
        {
            var blob = CloudStorageAccount.Parse(connectionString)
                .CreateCloudBlobClient()
                .GetContainerReference(containerName)
                .GetBlockBlobReference(blobName);

            var notes = "[[CHIEF'S STORY NOTES]]";
            await blob.UploadTextAsync(notes);
            Console.WriteLine($"The newsroom chief has saved story notes to the blob {containerName}/{blobName}");
        }

        // This method simulates what happens inside the news editing application when
        // a reporter loads a file in the editor, makes changes and saves it back
        private static async Task SimulateReporter(string authorName, TimeSpan writingTime)
        {
            // First, the reporter retrieves the current contents
            Console.WriteLine($"{authorName} begins work");
            var blob = CloudStorageAccount.Parse(connectionString)
                .CreateCloudBlobClient()
                .GetContainerReference(containerName)
                .GetBlockBlobReference(blobName);

            var contents = await blob.DownloadTextAsync();
            Console.WriteLine($"{authorName} loads the file and sees the following content: \"{contents}\"");

            // Next, the author writes their story. This takes some time.
            //var currentETag = blob.Properties.ETag;
            //Console.WriteLine($"\"{contents}\" has this ETag: {blob.Properties.ETag}");

            // Acquire a lease on the file
            Task<string> lease = null;
            try
            {
                lease = blob.AcquireLeaseAsync(TimeSpan.FromSeconds(15), null);
                Console.WriteLine($"{authorName} has acquired a lease on blob storage");
            }
            catch (Microsoft.WindowsAzure.Storage.StorageException e)
            {
                // Catch error if the ETag has changed it's value since opening the file
                Console.WriteLine($"{authorName} is unable to acquire a lease on the file and cannot continue. Error: {e.Message}");
            }


            Console.WriteLine($"{authorName} begins writing their story...");
            await Task.Delay(writingTime);
            Console.WriteLine($"{authorName} has finished writing their story");

            // Finally, they save their story back to the blob.
            var story = $"[[{authorName.ToUpperInvariant()}'S STORY]]";
            //await blob.UploadTextAsync(story);
            await blob.UploadTextAsync(story, null, accessCondition: AccessCondition.GenerateLeaseCondition(await lease), null, null);
            Console.WriteLine($"{authorName} has saved their story to Blob storage. New blob contents: \"{story}\"");
        }
    }
}
